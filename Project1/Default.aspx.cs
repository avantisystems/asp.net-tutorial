﻿using System;
using System.Web;
using System.Web.UI;

namespace Project1
{

    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HelloWorldLabel.Text = "Hello, world!";

            // These values can come from anywhere, but right now, we just hardcode them
          //  MyUserInfoBoxControl.UserName = "Jane Doe";
          //  MyUserInfoBoxControl.UserAge = 33;
          //  MyUserInfoBoxControl.UserCountry = "Germany";
        }
    }
}
