﻿<%@ Page Language="C#" Inherits="Project1.Default" %>
<!DOCTYPE html>
<script runat="server">

    protected void GreetButton_Click(object sender, EventArgs e)
    {
        HelloWorldLabel.Text = "Hello, " + TextInput.Text;
    }

    protected void GreetList_SelectedIndexChanged(object sender, EventArgs e)
    {
        HelloWorldLabel.Text = "Hello, " + GreetList.SelectedValue;
    }
</script>

<html>
<head runat="server">
	<title>Default</title>
</head>
<body>
	<form id="form1" runat="server">
        <asp:Label runat="server" id="HelloWorldLabel"></asp:Label>
        <br /><br />
        <asp:TextBox runat="server" id="TextInput" /> 
        <asp:Button runat="server" id="GreetButton" text="Say Hello!" OnClick="GreetButton_Click" />

        <asp:DropDownList runat="server" id="GreetList" autopostback="true" onselectedindexchanged="GreetList_SelectedIndexChanged">
            <asp:ListItem value="no one">No one</asp:ListItem>
            <asp:ListItem value="world">World</asp:ListItem>
            <asp:ListItem value="universe">Universe</asp:ListItem>
        </asp:DropDownList>    

       
	</form>
</body>
</html>
